/* PORTCHECK SOURCE CODE */

// Portcheck is a utility to make sure that all of your PBuilds/E-Builds
// are correct and can Be ran.

import std.stdio;
import std.process;
import std.path;
import std.string;
import std.file;

int print_usage(bool err = false) {
  writeln("
usage: portcheck <Directory>
");

  if (err == true) return 1;
  return 0;
}

int main(string[] args) {
  if (args.length == 1)
    writeln("'portcheck' requires a directory.");

  switch (args[1]) {
    case "help":
      return print_usage(false);
    default:
      if (!isDir(args[1])) {
        writeln("error: command/directory not found");
        return print_usage(true);
      }
      break;
  }

  writeln("Checking for PBuild/E-Build");

  if (exists("pbuild")) return check_pbs(args[1]);

  return 0;
}

int check_pbs(string dir) {
  int problems_found = 0;
  writeln("verifying integrity of `pbuild'");
  chdir(dir);
  if (!exists("pbuild")) { 
    writeln("error: integrity check failed");
    return -1;
  }
version (Windows) {
  writeln("error: win64/32 is not supported");
  return -1;
}
  writeln("sourcing spec.");
  
  string author = executeShell(". ./pbuild && echo ${author[0]}").output.strip;

  if (author.length == 0) {
    writeln("
portage/good-practice: 
  the `author' variable is not set.
    ");
    problems_found += 1;
  }

  auto instruction_set = executeShell(". ./pbuild && build");

  int instruct_status = instruction_set.status;

  if (instruct_status == 127) /* not found */ {
   writeln("
portage/portability:
  one or more commands in build() were not found.
  If you would like to support multiple builds, edit the file
  to use portable binaries, or pre-installed ones.

  Please note: Portage does not support external dependencies by default.
");
   problems_found += 1;
  }
  if (problems_found > 1) {
    writeln("There were more than one problems with your PBuild.");
    return 0;
  }
  if (problems_found == 1) {
    writeln("There was only one problem with your PBuild.");
  }
  writeln("There were no problems with your PBuild file.");
  return 0;
}

